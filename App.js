/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Button
} from 'react-native';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator,TransitionSpecs,HeaderStyleInterpolators} from 'react-navigation-stack';
import 'react-native-gesture-handler';

const config = {
  gestureDirection: 'horizontal',
  transitionSpec: {
    open: TransitionSpecs.TransitionIOSSpec,
    close: TransitionSpecs.TransitionIOSSpec,
  },
  headerStyleInterpolator: HeaderStyleInterpolators.forFade,
  cardStyleInterpolator: ({ current, next, layouts }) => {
    return {
      cardStyle: {
        transform: [
          {
            translateX: current.progress.interpolate({
              inputRange: [0, 1],
              outputRange: [layouts.screen.width+250, 0],
            }),
          },
          {
            rotate: current.progress.interpolate({
              inputRange: [0, 1],
              outputRange: [1, 0],
            }),
          },
          {
            scale: next
              ? next.progress.interpolate({
                  inputRange: [0, 1],
                  outputRange: [1, 0.9],
                })
              : 1,
          },
        ],
      },
      overlayStyle: {
        opacity: current.progress.interpolate({
          inputRange: [0, 1],
          outputRange: [0, 0.5],
        }),
      },
    };
  },
};

class Screen_1 extends Component{
  static navigationOptions = {
    title : "Home Screen",
    headerTransparent: true,
    headerTintColor: '#fff',
    headerStyle:{
      backgroundColor: 'rgba(0,0,0,0.3)'
    },
    gestureEnabled: true,
    ...config,
  }
  render(){
    const{navigate} = this.props.navigation;
    return(
      <View style={styles.Container_screen_1}>
        <Text style={styles.text}>THIS IS HOME SCREEN</Text>
        <Button title='NextPage' onPress={()=>navigate('Second')}/>
      </View>
    );
  }
}

class Screen_2 extends Component{
  static navigationOptions = {
    title : "Second Screen",
    headerTransparent: true,
    headerTintColor: '#fff',
    headerStyle:{
      backgroundColor: 'rgba(0,0,0,0.3)'
    },
    gestureEnabled: true,
    ...config,
  }
  render(){
    const{navigate} = this.props.navigation;
    return(
      <View style={styles.Container_screen_2}>
        <Text style={styles.text}>THIS IS PAGE 2</Text>
        <Button title='Go Back' onPress={()=>navigate('Home')}/>
        <Button title='Next Page' onPress={()=>navigate('Third')}/>
      </View>
    );
  }
}

class Screen_3 extends Component{
  static navigationOptions = {
    title : "Third Screen",
    headerTransparent: true,
    headerTintColor: '#fff',
    headerStyle:{
      backgroundColor: 'rgba(0,0,0,0.3)'
    },
    gestureEnabled: true,
    ...config,
  }
  render(){
    const{navigate} = this.props.navigation;
    return(
      <View style={styles.Container_screen_3}>
        <Text style = {styles.text}>THIS IS THE LAST PAGE</Text>
        <Button title='Go Back' onPress={()=>navigate('Second')}/>
        <Button title='Back to Home Screen' onPress={()=>navigate('Home')}/>
      </View>
    );
  }
}

const MainNavigator = createStackNavigator({
  Home: Screen_1,
  Second: Screen_2,
  Third: Screen_3
});



const styles = StyleSheet.create({
  Container_screen_1: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#FF1744',
    padding: 14
  },
  Container_screen_2: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#D500F9',
    padding: 14
  },
  Container_screen_3: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#1B5E20',
    padding: 14
  },
  text: {
    fontSize: 24,
    color: '#fff',
    textAlign: 'center',
    marginBottom: 20
  }
});

const App = createAppContainer(MainNavigator);

export default App;
